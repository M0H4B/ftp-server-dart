import 'dart:io';

Directory getApplicationDocumentsDirectory() {
  if (Platform.isMacOS) {
    var dir = Directory(Directory.current.path + '/Documents');
    if (!dir.existsSync()) dir.createSync(recursive: true);
    return dir;
  } else {
    return Directory.current;
  }
}

class PathProvider {
  static String getDirectoryName(Directory dir) {
    String path = dir.path;
    if (path.endsWith(Platform.pathSeparator)) {
      path = path.substring(0, path.length - 1);
    }
    return path.substring(path.lastIndexOf(Platform.pathSeparator) + 1);
  }

  static String getFileName(File file) {
    String path = file.path;
    if (path.endsWith(Platform.pathSeparator)) {
      path = path.substring(0, path.length - 1);
    }
    return path.substring(path.lastIndexOf(Platform.pathSeparator) + 1);
  }

  static Future<Directory> getHomeDirectory() async {
    if (Platform.isWindows) {
      return Directory.current.parent;
    }

    return getApplicationDocumentsDirectory();
  }

  static String combinePath(List<String> paths) {
    return paths.join(Platform.pathSeparator);
  }

  static FileSystemEntity? getFileSystemEntity(String path) {
    if (FileSystemEntity.isDirectorySync(path)) {
      return Directory(path);
    }
    if (FileSystemEntity.isFileSync(path)) {
      return File(path);
    }
    return null;
  }
}
