import 'dart:io';

import 'file_system_helper.dart';

Future<SecurityContext> securityContext() async {
  var certificate =
      await FileSystemHelper.readBytesFromAssets("assets/cert/certificate.pem");
  var caCertificate = await FileSystemHelper.readBytesFromAssets(
      "assets/cert/ca_certificate.pem");
  var privateKey =
      await FileSystemHelper.readBytesFromAssets("assets/cert/private_key.pem");
  SecurityContext securityContext = SecurityContext(
      //withTrustedRoots: true
      );

  securityContext
      .setTrustedCertificatesBytes(caCertificate.buffer.asUint8List());

  securityContext.useCertificateChainBytes(certificate.buffer.asUint8List());

  securityContext.usePrivateKeyBytes(privateKey.buffer.asUint8List(),
      password: '123456');
  securityContext.setAlpnProtocols([
    'TLSv1.2',
    'TLSv1.3',
  ], true);
  return securityContext;
}

Future<SecurityContext> clientSecurityContext() async {
  var caCertificate = await FileSystemHelper.readBytesFromAssets(
      "assets/cert/ca_certificate.pem");
  SecurityContext securityContext = SecurityContext(
      //withTrustedRoots: true
      );

  securityContext
      .setTrustedCertificatesBytes(caCertificate.buffer.asUint8List());

  securityContext.setAlpnProtocols([
    'TLSv1.2',
    'TLSv1.3',
  ], false);
////
  return securityContext;
}

Future<SecurityContext> securityContext2() async {
  var certificate =
      await FileSystemHelper.readBytesFromAssets("assets/cert/certificate.pem");
  var caCertificate = await FileSystemHelper.readBytesFromAssets(
      "assets/cert/ca_certificate.pem");
  var privateKey =
      await FileSystemHelper.readBytesFromAssets("assets/cert/private_key.pem");
  SecurityContext securityContext = SecurityContext(
      //withTrustedRoots: true
      );

  securityContext
      .setTrustedCertificatesBytes(caCertificate.buffer.asUint8List());

  securityContext.useCertificateChainBytes(certificate.buffer.asUint8List());

  securityContext.usePrivateKeyBytes(privateKey.buffer.asUint8List(),
      password: '123456');
  securityContext.setAlpnProtocols([
    'TLSv1.2',
    'TLSv1.3',
  ], false);
  return securityContext;
}
