import 'dart:io';
import 'dart:typed_data';

class FileSystemHelper {
  static Future<ByteData> readBytesFromAssets(String file) async {
    try {
      File _file = File(file);
      Uint8List bytes = await _file.readAsBytes();
      return ByteData.view(bytes.buffer);
    } catch (e) {
      throw Exception('Failed to read file: $e');
    }
  }

  static String getMlsdEntry(FileSystemEntity entity) {
    final isDirectory = entity is Directory;
    final name = entity.path.split('/').last;
    final modified = entity.statSync().modified.toUtc().toIso8601String();

    final entryType = isDirectory ? 'type=dir' : 'type=file';
    final entrySize = isDirectory ? '' : 'size=${entity.statSync().size}';
    final entryModify = 'modify=$modified';
    final entryPerm = 'perm=el';

    return '$entryType;$entrySize;$entryModify;$entryPerm; $name';
  }

  static String getListEntry2(FileSystemEntity entity) {
    String fileName = entity.path.split(Platform.pathSeparator).last;
    final FileStat stat = entity.statSync();
    final int fileSize = entity.statSync().size;
    final String filePermissions = FileSystemHelper._getFilePermissions(entity);
    final DateTime lastModified = entity.statSync().modified;
    final String formattedLastModified =
        lastModified.toUtc().toString().split(' ').first;
    if (stat.type == FileSystemEntityType.directory)
      fileName = fileName + Platform.pathSeparator;

    return '$filePermissions\t_\t_\t_\t$fileSize\t${lastModified.month.toString().padLeft(2, '0')} '
        '${lastModified.day.toString().padLeft(2, '0')}\t${lastModified.hour.toString().padLeft(2, '0')}:${lastModified.minute.toString().padLeft(2, '0')}\t$fileName\r\n';
  }

  static String _getFilePermissions(FileSystemEntity entity) {
    final FileStat fileStat = entity.statSync();

    final int mode = fileStat.mode;
    final StringBuffer permissions = StringBuffer();
    if (fileStat.type == FileSystemEntityType.directory)
      permissions.write('d');
    else
      permissions.write('-');
    permissions.write(_getPermissionString((mode >> 6) & 7));
    permissions.write(_getPermissionString((mode >> 3) & 7));
    permissions.write(_getPermissionString(mode & 7));

    return permissions.toString();
  }

  static String _getPermissionString(int value) {
    switch (value) {
      case 0:
        return '---';
      case 1:
        return '--x';
      case 2:
        return '-w-';
      case 3:
        return '-wx';
      case 4:
        return 'r--';
      case 5:
        return 'r-x';
      case 6:
        return 'rw-';
      case 7:
        return 'rwx';
      default:
        throw Exception('Invalid permission value');
    }
  }

  static Future<String> getListEntry(FileSystemEntity entity) async {
    String type = await FileSystemEntity.isDirectory(entity.path) ? 'd' : '-';
    String permissions = _getPermissions(entity);
    int links = await _getLinks(entity);
    String owner = await _getOwner(entity);
    String group = await _getGroup(entity);
    int size = await _getSize(entity);
    DateTime modified = await _getModified(entity);
    String name = entity.path.split(Platform.pathSeparator).last;

    String listing =
        '$permissions $links $owner\t$group\t$size ${modified.month.toString().padLeft(2, '0')} '
        '${modified.day.toString().padLeft(2, '0')} ${modified.hour.toString().padLeft(2, '0')}:${modified.minute.toString().padLeft(2, '0')}\t$name';

    return listing;
  }

  static String _getPermissions(FileSystemEntity file) {
    var stat = file.statSync();
    var mode = stat.modeString();
    return mode.substring(1, 10);
  }

  static Future<int> _getLinks(FileSystemEntity file) async {
    var stat = await file.stat();
    return 0;
  }

  static Future<String> _getOwner(FileSystemEntity file) async {
    var stat = await file.stat();
    // var userId = stat.stat.st_uid;
    return 'mohab';
  }

  static Future<String> _getGroup(FileSystemEntity file) async {
    var stat = await file.stat();
    // var groupId = stat.stat.st_gid;
    return 'stuff';
  }

  static Future<int> _getSize(FileSystemEntity file) async {
    var stat = await file.stat();
    return stat.size;
  }

  static Future<DateTime> _getModified(FileSystemEntity file) async {
    var stat = await file.stat();
    return stat.modified;
  }

  static FileSystemEntity? getFileSystemEntity(String path) {
    if (FileSystemEntity.isDirectorySync(path)) {
      return Directory(path);
    }
    if (FileSystemEntity.isFileSync(path)) {
      return File(path);
    }
    return null;
  }
}
