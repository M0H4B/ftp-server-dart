enum DataConnectionMode {
  Passive,
  Active,
  ExtendedPassive,
  ExtendedActive,
}
