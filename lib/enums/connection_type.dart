enum ConnectionType {
  command,
  upload,
  download,
  data,
  uploadSecure,
  downloadSecure,
  dataSecure,
}
