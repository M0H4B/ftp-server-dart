import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ftp_server/utils/security_context.dart';

import 'ftp_server/ftp_server.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Future.wait([
    FtpServer.startFTPServer(),
    FtpServer.startSecureFTPServer(),
  ]);
}
