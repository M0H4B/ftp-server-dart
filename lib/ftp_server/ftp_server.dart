import 'dart:async';
import 'dart:io';

import 'package:ftp_server/utils/security_context.dart';

import 'client_handler.dart';

class FtpServer {
  static Future<void> startSecureFTPServer() async {
    while (true) {
      try {
        final SecureServerSocket server = await SecureServerSocket.bind(
          InternetAddress.anyIPv4,
          990,
          await securityContext(),
          supportedProtocols: [
            'TLSv1.2',
            'TLSv1.3',
          ],
        );
        print(
            'Secure FTP server started on ${server.address.address}:${server.port}');

        await for (SecureSocket socket in server) {
          runZonedGuarded(
              () => ClientHandler(
                    socket,
                  ).handleClient().catchError((error) {
                    print('Close connection with client due to error: $error');
                    socket.close();
                  }), (error, stack) {
            print('Close connection with client due to error: $error');
          });
        }
      } catch (e) {
        print('FTP server Failed to start ${e}');
      }
    }
  }

  static Future<void> startFTPServer() async {
    while (true) {
      try {
        final ServerSocket server =
            await ServerSocket.bind(InternetAddress.anyIPv4, 21);
        print('FTP server started on ${server.address.address}:${server.port}');

        await for (Socket socket in server) {
          runZonedGuarded(
              () => ClientHandler(socket).handleClient().catchError((error) {
                    print('Close connection with client due to error: $error');
                    socket.close();
                  }), (error, stack) {
            print('Close connection with client due to error: $error');
          });
        }
      } catch (e) {
        print('FTP server Failed to start');
      }
    }
  }

  static ServerSocket? serverSocket;
}

// const showLineNumbers = true;
// var lineNumber = 1;
// var stream = File('quotes.txt').openRead();

// stream.transform(utf8.decoder)
//       .transform(const LineSplitter())
//       .forEach((line) {
//         if (showLineNumbers) {
//           stdout.write('${lineNumber++} ');
//         }
//         stdout.writeln(line);
//       });

