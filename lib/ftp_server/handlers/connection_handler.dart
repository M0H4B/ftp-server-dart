import 'dart:io';
import 'package:ftp_server/enums/connection_type.dart';
import 'package:ftp_server/enums/data_connection_mode.dart';
import 'package:ftp_server/enums/reply_code.dart';
import 'package:ftp_server/enums/transfare_mode.dart';
import 'package:ftp_server/ftp_server/handlers/command_handler.dart';
import 'package:ftp_server/utils/security_context.dart';
import 'package:mutex/mutex.dart';

class ConnectionHandler extends CommandHandler {
  Socket? _dataConnection;
  List<DataSink> dataConnections = [];

  bool secureTransfer = false;
  int bufferSize = -1;

  // * Stupid solution, because there is no accept method in socket server
  final Mutex _dataConnectionLock = Mutex();

  ConnectionHandler(Socket socket) : super(socket);

  Future<DataSink> getDataConnection(ConnectionType type,
      {TransferMode mode = TransferMode.ASCII}) async {
    return await _dataConnectionLock.protect<DataSink>(() async {
      while (_dataConnection == null) {
        await Future.delayed(const Duration(milliseconds: 10));
      }
      print(
          "New ${type}: ${_dataConnection!.remoteAddress.address}:${_dataConnection!.remotePort}\r\n");
      if (secureTransfer) {
        try {
          if (!(_dataConnection is SecureSocket)) {
            _dataConnection = await await SecureSocket.secureServer(
              commandSink,
              await securityContext(),
              supportedProtocols: [
                'TLSv1.2',
                'TLSv1.3',
              ],
            );
          }
        } catch (e) {
          print("Secure data connection error ${e}");
        }
      }
      var con = DataSink(type, _dataConnection!, mode: mode);
      dataConnections.add(con);
      _dataConnection = null;
      return con;
    });
  }

  Future<void> _handlePortCommand(
      InternetAddress userActiveIP, int userActiveDataPort) async {
    try {
      if (secureTransfer) {
        _dataConnection = await SecureSocket.connect(
          userActiveIP,
          userActiveDataPort,
          onBadCertificate: (certificate) => true,
        );
        await replyAsync(ReplyCode.CommandOkay, "PORT command successful.");
      } else {
        _dataConnection =
            await Socket.connect(userActiveIP, userActiveDataPort);
        await replyAsync(ReplyCode.CommandOkay, "PORT command successful.");
      }
    } catch (e) {
      await replyAsync(ReplyCode.BadSequence, "PORT command unsuccessful.");
    }
  }

  // @ ServerSocketBase
  dynamic serverSocket;
  Future<void> _handlePasvCommand(InternetAddress ipAddress, int port) async {
    if (secureTransfer) {
      try {
        serverSocket = await SecureServerSocket.bind(
          ipAddress,
          port,
          await securityContext(),
          supportedProtocols: [
            'TLSv1.2',
            'TLSv1.3',
          ],
        );
      } catch (e) {
        await replyAsync(ReplyCode.BadSequence, "PASV command unsuccessful.");
      }
    } else {
      serverSocket = await ServerSocket.bind(ipAddress, port);
    }
    serverSocket!.listen((client) {
      _dataConnection = client;
    }, onError: (e) async {
      print(e);
      serverSocket?.close();
      serverSocket = null;
    });
  }

  Future<void> openDataConnectionAsync(InternetAddress ipAddress, int port,
      {DataConnectionMode dataConnectionMode =
          DataConnectionMode.Passive}) async {
    switch (dataConnectionMode) {
      case DataConnectionMode.Active:
      case DataConnectionMode.ExtendedActive:
        await _handlePortCommand(ipAddress, port);
        break;
      case DataConnectionMode.Passive:
      case DataConnectionMode.ExtendedPassive:
        await _handlePasvCommand(ipAddress, port);
        break;
    }
  }
}

class DataSink {
  final ConnectionType type;
  final TransferMode mode;
  final Socket socket;
  DataSink(this.type, this.socket, {this.mode = TransferMode.ASCII});
  void send(String data) {
    if (mode == TransferMode.ASCII)
      socket.write(data);
    else
      socket.add(data.codeUnits);
  }

  Future<dynamic> flush() async {
    await socket.flush();
  }

  Future<dynamic> close() async {
    await socket.close();
  }
}
