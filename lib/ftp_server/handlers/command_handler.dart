import 'dart:io';

import 'package:ftp_server/enums/reply_code.dart';

class CommandHandler {
  Socket commandSink;
  CommandHandler(this.commandSink);

  Future<void> replyAsync(ReplyCode code, String message) async {
    try {
      final stringBuilder = StringBuffer()
        ..write('${code.value} ')
        ..write(message)
        ..write('\r\n');

      print(
          '${commandSink.remoteAddress.address}:${commandSink.remotePort}: $stringBuilder');

      commandSink.writeln(stringBuilder.toString());
      await commandSink.flush();
    } catch (_) {}
  }
}
