import 'dart:io';
import 'package:ftp_server/enums/connection_type.dart';
import 'package:ftp_server/enums/list_command.dart';
import 'package:ftp_server/enums/reply_code.dart';
import 'package:ftp_server/enums/transfare_mode.dart';
import 'package:ftp_server/ftp_server/handlers/connection_handler.dart';
import 'package:ftp_server/utils/file_system_helper.dart';

class TransferHandler extends ConnectionHandler {
  TransferHandler(Socket socket) : super(socket);

  Future<bool> uploadFile(File file) async {
    DataSink? _dataSink;
    try {
      _dataSink = await getDataConnection(secureTransfer
              ? ConnectionType.uploadSecure
              : ConnectionType.upload)
          .timeout(const Duration(seconds: 30));
      await _dataSink.socket.addStream(file.openRead());
      await _dataSink.flush();
      await replyAsync(
          ReplyCode.SuccessClosingDataConnection, "Transfer complete");
      return true;
    } catch (e) {
      print("$e\r\n");
      return false;
    } finally {
      await _dataSink?.close();
      dataConnections.remove(_dataSink);
    }
  }

  Future<bool> downloadFile(File file) async {
    DataSink? _dataSink;
    try {
      _dataSink = await getDataConnection(secureTransfer
              ? ConnectionType.downloadSecure
              : ConnectionType.download)
          .timeout(const Duration(seconds: 30));
      await file.openWrite().addStream(_dataSink.socket);
      await _dataSink.flush();
      await replyAsync(
          ReplyCode.SuccessClosingDataConnection, "Transfer complete");
      return true;
    } catch (e) {
      print("$e\r\n");
      return false;
    } finally {
      await _dataSink?.close();
      dataConnections.remove(_dataSink);
    }
  }

  Future<bool> sendDirectoryList(
      Directory directory, ListCommand command, TransferMode mode) async {
    DataSink? _dataSink;
    try {
      _dataSink = await getDataConnection(
        secureTransfer ? ConnectionType.dataSecure : ConnectionType.data,
        mode: mode,
      ).timeout(const Duration(seconds: 20));

      final List<FileSystemEntity> files = directory.listSync();
      for (FileSystemEntity file in files) {
        String fileDetails;
        if (command == ListCommand.MLSD)
          fileDetails = FileSystemHelper.getMlsdEntry(file);
        else {
          fileDetails = FileSystemHelper.getListEntry2(file);
        }
        _dataSink.send(fileDetails);
        await _dataSink.flush();
      }
      await _dataSink.flush();
      await Future.delayed(Duration(seconds: 1));
      return true;
    } catch (e) {
      print("$e\r\n");
      return false;
    } finally {
      await _dataSink?.close();
      dataConnections.remove(_dataSink);
    }
  }

  Future<void> forceCloseConnections() async {
    for (var sink in dataConnections) {
      try {
        await sink.close();
      } catch (_) {
      } finally {
        dataConnections.remove(sink);
      }
    }
    await serverSocket?.close();
    serverSocket = null;
  }
}
