import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:ftp_server/enums/data_connection_mode.dart';
import 'package:ftp_server/enums/list_command.dart';
import 'package:ftp_server/enums/reply_code.dart';
import 'package:ftp_server/enums/transfare_mode.dart';
import 'package:ftp_server/ftp_server/handlers/transfer_handler.dart';
import 'package:ftp_server/utils/file_system_helper.dart';
import 'package:ftp_server/utils/path_provider.dart';
import 'package:ftp_server/utils/security_context.dart';
import '../auth/auth.dart';
import 'package:path/path.dart' as path;

class ClientHandler extends TransferHandler with Auth {
  ClientHandler(Socket socket) : super(socket) {
    print(
        "New client connected: ${socket.remoteAddress.address}:${socket.remotePort}\r\n");

    replyAsync(ReplyCode.ServiceReady, "Welcome to Dart FTP Server");
  }
  String? renameFromPath;
  TransferMode transferMode = TransferMode.ASCII;

  Future<void> handleClient() async {
    Directory currentDirectory = (await PathProvider.getHomeDirectory());
    String username = 'anonymous';
    try {
      await for (String line
          in utf8.decoder.bind(commandSink).transform(const LineSplitter())) {
        final List<String> parts = line.split(' ');
        final String command = parts[0].toUpperCase();
        final String argument =
            parts.length > 1 ? parts.sublist(1).join(' ') : '';
        switch (command) {
          case 'AUTH':
            final String authType = parts[1].toUpperCase();
            if (authType == 'TLS') {
              if (!(commandSink is SecureSocket)) {
                await replyAsync(
                    ReplyCode.ProceedWithNegotiation, "AUTH TLS successful.");
                commandSink = await SecureSocket.secureServer(
                  commandSink,
                  await securityContext(),
                  supportedProtocols: [
                    'TLSv1.2',
                    'TLSv1.3',
                  ],
                );

                handleClient();
                return;
              } else {
                (commandSink as SecureSocket).renegotiate();
              }
            } else {
              await replyAsync(ReplyCode.ParameterNotImplemented,
                  "Command not implemented for that parameter.");
            }
            break;
          case 'USER':
            username = argument;
            await replyAsync(
                ReplyCode.NeedPassword, "Password required for $username");
            break;
          case 'PASS':
            if (authenticateUser(username, argument)) {
              await replyAsync(
                  ReplyCode.UserLoggedIn, "User $username logged in");
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Login incorrect");
            }
            break;
          case 'SYST':
            await replyAsync(ReplyCode.NameSystemType, "UNIX Type: L8");
            break;
          case 'FEAT':
            commandSink.writeln('211-Features supported:');
            commandSink.writeln(' SIZE');
            commandSink.writeln(' MDTM');
            commandSink.writeln(' PASV');
            commandSink.writeln(' PORT');
            commandSink.writeln(' AUTH TLS');
            commandSink.writeln(' USER');
            commandSink.writeln(' PASS');
            commandSink.writeln(' SYST');
            commandSink.writeln(' PWD');
            commandSink.writeln(' TYPE');
            commandSink.writeln(' CWD');
            commandSink.writeln(' LIST');
            commandSink.writeln(' RETR');
            commandSink.writeln(' STOR');
            commandSink.writeln(' DELE');
            commandSink.writeln(' MKD');
            commandSink.writeln(' RMD');
            commandSink.writeln('211 End');
            break;
          case 'PWD':
            if (authenticated) {
              await replyAsync(
                  ReplyCode.PathCreated, '"${currentDirectory.path}"');
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }
            break;
          case 'TYPE':
            if (argument.toUpperCase().startsWith("I")) {
              transferMode = TransferMode.IMAGE;
            } else if (argument.toUpperCase().startsWith("L")) {
              transferMode = TransferMode.LOCAL;
            } else if (argument.toUpperCase().startsWith("E")) {
              transferMode = TransferMode.EBCDIC;
            } else {
              transferMode = TransferMode.ASCII;
            }
            await replyAsync(ReplyCode.CommandOkay, "Type set to ${argument}");
            break;
          case 'CWD':
            if (authenticated) {
              String dirPath = argument;
              if (path.isRelative(dirPath)) {
                dirPath =
                    currentDirectory.path + Platform.pathSeparator + argument;
              }
              final Directory newDirectory = Directory(dirPath);
              try {
                var list = newDirectory.listSync();
                currentDirectory = newDirectory;
                await replyAsync(ReplyCode.FileActionOk,
                    "Directory changed to ${currentDirectory.path}");
              } catch (e) {
                await replyAsync(ReplyCode.FileNoAccess,
                    "Permission or Directory not found $e");
              }
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }
            break;
          case 'LIST':
            if (authenticated) {
              try {
                await replyAsync(ReplyCode.AboutToOpenDataConnection,
                    "Opening data connection for LIST.");
                var result = await sendDirectoryList(
                    currentDirectory, ListCommand.LIST, transferMode);
                if (result) {
                  await replyAsync(ReplyCode.SuccessClosingDataConnection,
                      "Directory listing completed");
                } else {
                  await replyAsync(
                      ReplyCode.BadSequence, "LIST command unsuccessful.");
                }
              } catch (e) {
                await replyAsync(ReplyCode.FileNoAccess,
                    "You don't have permission to access ${currentDirectory.path}");
              }
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }
            break;
          case 'RETR':
            if (authenticated) {
              final File file = File('${currentDirectory.path}/$argument');
              if (file.existsSync()) {
                await replyAsync(ReplyCode.AboutToOpenDataConnection,
                    "Opening data connection for RETR.");
                await uploadFile(file);
              } else {
                await replyAsync(ReplyCode.FileNoAccess, "File not found");
              }
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }
            break;
          case 'STOR':
            if (authenticated) {
              await replyAsync(ReplyCode.AboutToOpenDataConnection,
                  "Opening data connection for STOR.");
              await downloadFile(File('${currentDirectory.path}/$argument'));
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }
            break;
          case 'DELE':
            if (authenticated) {
              final File file = File('${currentDirectory.path}/$argument');
              if (file.existsSync()) {
                file.deleteSync();
                await replyAsync(ReplyCode.FileActionOk, "File deleted");
              } else {
                await replyAsync(ReplyCode.FileNoAccess, "File not found");
              }
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }
            break;
          case 'MKD':
            if (authenticated) {
              final Directory newDirectory =
                  Directory('${currentDirectory.path}/$argument');
              newDirectory.createSync();
              await replyAsync(ReplyCode.PathCreated, "Directory created");
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }
            break;
          case 'RNFR':
            renameFromPath = null;
            if (authenticated) {
              final entity = PathProvider.getFileSystemEntity(
                  '${currentDirectory.path}/$argument');
              if (entity != null && await entity.exists()) {
                await replyAsync(ReplyCode.FileActionPendingInfo,
                    "File exists, ready for destination name");
                renameFromPath = entity.absolute.path;
              } else {
                await replyAsync(ReplyCode.FileNoAccess, "File not found");
              }
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }
            break;

          case 'RNTO':
            if (authenticated) {
              if (renameFromPath == null) {
                await replyAsync(
                    ReplyCode.BadSequence, "Bad sequence of commands");
                return;
              }
              try {
                var fileSystemEntity =
                    PathProvider.getFileSystemEntity(renameFromPath!);
                if (fileSystemEntity != null) {
                  FileSystemEntity renameFrom = fileSystemEntity;
                  String renameTo = argument.replaceAll(' ', '\ ');
                  if (path.isRelative(renameTo)) {
                    // rename

                    renameTo = renameFrom.parent.path +
                        Platform.pathSeparator +
                        renameTo;
                  }

                  if (await renameFrom.exists()) {
                    await renameFrom.rename(renameTo);
                    await replyAsync(
                        ReplyCode.FileActionOk, "File renamed successfully");
                  }
                } else
                  await replyAsync(ReplyCode.FileNoAccess, "File not found");
              } catch (_) {
                await replyAsync(ReplyCode.FileNoAccess, "File not found");
              }
              renameFromPath = null;
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }

            break;
          case 'RMD':
            if (authenticated) {
              final Directory directory =
                  Directory('${currentDirectory.path}/$argument');
              if (directory.existsSync()) {
                directory.deleteSync(recursive: true);
                await replyAsync(ReplyCode.FileActionOk, "Directory deleted");
              } else {
                await replyAsync(ReplyCode.FileNoAccess, "Directory not found");
              }
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }
            break;
          case 'QUIT':
            commandSink.writeln('221 Goodbye');
            await commandSink.flush();
            await commandSink.close();
            print(
                "Client disconnected: ${commandSink.remoteAddress.address}:${commandSink.remotePort}\r\n");
            return;
          case 'PORT':
            final List<String> portParts = parts[1].split(',');
            if (portParts.length != 6) {
              await replyAsync(ReplyCode.SyntaxErrorInParametersOrArguments,
                  "Syntax error, count of comma incorrect");
              return;
            }
            var userActiveIP = portParts.sublist(0, 4).join('.');
            var userActiveDataPort =
                (int.parse(portParts[4]) << 8) + int.parse(portParts[5]);
            await openDataConnectionAsync(
                InternetAddress.tryParse(userActiveIP)!, userActiveDataPort,
                dataConnectionMode: DataConnectionMode.Active);
            break;
          case 'PASV':
            if (serverSocket == null) {
              await openDataConnectionAsync(InternetAddress.anyIPv4, 0,
                  dataConnectionMode: DataConnectionMode.Passive);
            }
            final String pasvAddress =
                serverSocket!.address.address.replaceAll('.', ',');
            final int serverPort = serverSocket!.port;
            final String pasvPort = (serverPort ~/ 256).toString() +
                ',' +
                (serverPort % 256).toString();
            await replyAsync(ReplyCode.EnteringPassiveMode,
                "Entering Passive Mode ($pasvAddress,$pasvPort).");

            // await replyAsync(ReplyCode.CommandOkay, "PASV command successful.");
            await commandSink.flush();
            break;
          case 'SIZE':
            final String fileName = parts[1];
            final File file =
                File(currentDirectory.path + Platform.pathSeparator + fileName);
            final bool fileExists = await file.exists();

            if (fileExists) {
              final int fileSize = await file.length();
              commandSink.write('213 $fileSize\r\n');
            } else {
              await replyAsync(ReplyCode.FileNoAccess, "File not found");
            }
            break;
          case 'MLSD':
            if (authenticated) {
              await replyAsync(ReplyCode.AboutToOpenDataConnection,
                  "Opening data connection for MLSD.");
              var result = await sendDirectoryList(
                  currentDirectory, ListCommand.MLSD, transferMode);
              if (result) {
                await replyAsync(ReplyCode.SuccessClosingDataConnection,
                    "Transfer complete");
                await replyAsync(
                    ReplyCode.FileActionOk, "End of MLSD listing.");
              } else {
                await replyAsync(
                    ReplyCode.BadSequence, "MLSD command unsuccessful.");
              }
            } else {
              await replyAsync(ReplyCode.NotLoggedIn, "Not logged in");
            }

            break;
          // case 'PBSZ':
          //   if (parts.length < 2) {
          //     await replyAsync(ReplyCode.SyntaxErrorInParametersOrArguments,
          //         "Syntax error in parameters or arguments");
          //     return;
          //   }

          //   int requestedBufferSize;
          //   try {
          //     requestedBufferSize = int.parse(parts[1]);
          //   } catch (e) {
          //     await replyAsync(ReplyCode.SyntaxErrorInParametersOrArguments,
          //         "Invalid buffer size");
          //     return;
          //   }

          //   if (requestedBufferSize < 0) {
          //     await replyAsync(ReplyCode.SyntaxErrorInParametersOrArguments,
          //         "Invalid buffer size");
          //     return;
          //   }
          //   // Handle the PBSZ command here
          //   // You can use the bufferSize value as needed
          //   bufferSize = requestedBufferSize;

          //   await replyAsync(ReplyCode.CommandOkay, "PBSZ command successful.");
          //   break;
          // case 'PROT':
          //   if (parts.length < 2) {
          //     await replyAsync(ReplyCode.SyntaxErrorInParametersOrArguments,
          //         "Syntax error in parameters or arguments");
          //     return;
          //   }

          //   String requestedProtectionLevel = parts[1].toUpperCase();
          //   if (requestedProtectionLevel != 'C' &&
          //       requestedProtectionLevel != 'P') {
          //     await replyAsync(ReplyCode.ParameterNotImplemented,
          //         "Command not implemented for that parameter.");
          //     return;
          //   }
          //   secureTransfer = requestedProtectionLevel == 'P';
          //   await replyAsync(ReplyCode.CommandOkay, "PROT command successful.");
          //   break;
          // case 'MDTM':
          //   if (parts.length < 2) {
          //     await replyAsync(ReplyCode.SyntaxErrorInParametersOrArguments,
          //         "Syntax error in parameters or arguments");
          //     return;
          //   }

          //   String filePath = parts[1];

          //   File file = File(filePath);
          //   if (!file.existsSync()) {
          //     await replyAsync(ReplyCode.FileNoAccess, "File not found");

          //     return;
          //   }

          //   DateTime modifiedTime = file.lastModifiedSync();
          //   String formattedTime = formatDate(modifiedTime);
          //   await replyAsync(ReplyCode.timestamp, formattedTime);
          //   break;
          default:
            await replyAsync(
                ReplyCode.NotImplemented, "Command not implemented");
        }

        await commandSink.flush();
      }
    } catch (e) {
      print("$e\r\n");

      replyAsync(ReplyCode.LocalError, "Force connection close");

      await forceCloseConnections();
      await commandSink.close();
      return;
    }
  }

  String formatDate(DateTime dateTime) {
    String year = dateTime.year.toString().padLeft(4, '0');
    String month = dateTime.month.toString().padLeft(2, '0');
    String day = dateTime.day.toString().padLeft(2, '0');
    String hour = dateTime.hour.toString().padLeft(2, '0');
    String minute = dateTime.minute.toString().padLeft(2, '0');
    String second = dateTime.second.toString().padLeft(2, '0');

    return '$year$month$day$hour$minute$second';
  }

  Future<void> sendBytes(Socket clientSocket, List<int> data) async {
    await clientSocket.addStream(Stream.fromIterable([data]));
    await clientSocket.flush();
  }

  Future<void> sendString(Socket clientSocket, String data) async {
    clientSocket.writeln(data);
    await clientSocket.flush();
  }
}
