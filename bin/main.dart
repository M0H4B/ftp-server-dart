import 'dart:async';

import 'package:ftp_server/ftp_server/ftp_server.dart';

void main() async {
  await Future.wait([
    FtpServer.startFTPServer(),
    FtpServer.startSecureFTPServer(),
  ]);
}
